<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('data.form');
    }

    public function login(Request $request) {
        $first = $request['first'];
        $last = $request['last'];
        return view('index', compact('first', 'last'));
    }

}
